<?php
class OneSpace_OsFuncoes_Helper_Data extends Mage_Core_Helper_Abstract
{
  private function calculateMinNumParcelas($preco, $max_de_parcelas, $parcela_minima) {
    if ($preco < $parcela_minima) return 1;
    while ($preco / $max_de_parcelas < $parcela_minima) {
      $max_de_parcelas--;
    }

    return $max_de_parcelas;
  }

  public function parcelamento($produto)
  {
    $num_parcelas = (int) Mage::getModel('core/variable')->loadByCode('num_parcelas')->getValue('plain');
    $parcela_minima = (int) Mage::getModel('core/variable')->loadByCode('parcela_minima')->getValue('plain');
    $preco_normal = $produto->getPrice();
    $preco_special = $produto->getSpecialPrice();
    $preco = isset($preco_special) ? $preco_special : $preco_normal;
    $max_de_parcelas = $this->calculateMinNumParcelas($preco, $num_parcelas, $parcela_minima);

    if ($max_de_parcelas <= 1) {
        $num_parcelas = 1;
        return '<p class="parcelamento"></p>';
    } elseif($max_de_parcelas <= $num_parcelas) {
        $num_parcelas = $max_de_parcelas;
    }

    $parcela = $preco / $num_parcelas;
    $parcela = Mage::getModel('directory/currency')->format($parcela);

    $mensagem = '<p class="parcelamento">Em até ' . $num_parcelas . 'x de ' . $parcela . ' sem juros no cartão</p>';

    return $mensagem;
  }
}
